package ru.t1.zkovalenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.endpoint.IProjectEndpoint;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.dto.model.ProjectDTO;
import ru.t1.zkovalenko.tm.dto.request.project.*;
import ru.t1.zkovalenko.tm.enumerated.Sort;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.marker.IntegrationCategory;
import ru.t1.zkovalenko.tm.service.PropertyService;

import java.util.List;

import static ru.t1.zkovalenko.tm.constant.EndpointTestData.USER_ADMIN_LOGIN;
import static ru.t1.zkovalenko.tm.constant.EndpointTestData.USER_ADMIN_PASSWORD;
import static ru.t1.zkovalenko.tm.enumerated.Status.*;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest extends AbstractEndpointTest {

    @Nullable
    private String userToken;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private ProjectDTO testProject;

    @Nullable
    private ProjectDTO testProjectCreate() {
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken);
        createRequest.setName("PROJECT_NAME");
        createRequest.setDescription("Description");
        @Nullable final ProjectDTO projectCreated = projectEndpoint.createProject(createRequest).getProject();
        return projectCreated;
    }

    @Nullable
    private ProjectDTO projectFindById(final String Id) {
        @NotNull final ProjectShowByIdRequest showByIdRequest = new ProjectShowByIdRequest(userToken);
        showByIdRequest.setProjectId(Id);
        @Nullable final ProjectDTO projectFounded = projectEndpoint.showByIdProject(showByIdRequest).getProject();
        return projectFounded;
    }

    @Nullable
    private ProjectDTO projectFindByIndex(final Integer Index) {
        @NotNull final ProjectShowByIndexRequest showByIndexRequest = new ProjectShowByIndexRequest(userToken);
        showByIndexRequest.setIndex(Index);
        @Nullable final ProjectDTO projectFounded = projectEndpoint.showByIndexProject(showByIndexRequest).getProject();
        return projectFounded;
    }

    @Before
    public void before() {
        userToken = login(USER_ADMIN_LOGIN, USER_ADMIN_PASSWORD);
        saveBackup(userToken);
        serverAutoBackup(false, userToken);
        testProject = testProjectCreate();
    }

    @After
    public void after() {
        loadBackup(userToken);
        serverAutoBackup(true, userToken);
        logout(userToken);
    }

    @Test
    public void projectChangeStatusById() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken);
        request.setProjectId(testProject.getId());
        request.setStatus(COMPLETED);
        projectEndpoint.changeStatusByIdProject(request);
        @Nullable ProjectDTO projectFounded = projectFindById(testProject.getId());
        Assert.assertEquals(projectFounded.getStatus(), COMPLETED);
    }

    @Test
    public void projectChangeStatusByIndex() {
        @NotNull final Status statusBefore = projectFindByIndex(1).getStatus();
        @NotNull final Status statusToSet = statusBefore == COMPLETED ? NOT_STARTED : COMPLETED;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(userToken);
        request.setIndex(1);
        request.setStatus(statusToSet);
        final ProjectDTO projectChanged = projectEndpoint.changeStatusByIndexProject(request).getProject();
        Assert.assertEquals(projectChanged.getStatus(), statusToSet);
    }

    @Test
    public void clearProject() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(request);
        Assert.assertNull(projectFindById(testProject.getId()));
    }

    @Test
    public void projectCompleteById() {
        @NotNull final ProjectChangeStatusByIdRequest requestChange = new ProjectChangeStatusByIdRequest(userToken);
        requestChange.setProjectId(testProject.getId());
        requestChange.setStatus(IN_PROGRESS);
        projectEndpoint.changeStatusByIdProject(requestChange);
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(userToken);
        request.setProjectId(testProject.getId());
        projectEndpoint.completeByIdProject(request);
        @Nullable final ProjectDTO projectFounded = projectFindById(testProject.getId());
        Assert.assertEquals(projectFounded.getStatus(), COMPLETED);
    }

    @Test
    public void projectCompleteByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest requestChange = new ProjectChangeStatusByIndexRequest(userToken);
        requestChange.setIndex(1);
        requestChange.setStatus(IN_PROGRESS);
        projectEndpoint.changeStatusByIndexProject(requestChange);
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(userToken);
        request.setIndex(1);
        final ProjectDTO projectChanged = projectEndpoint.completeByIndexProject(request).getProject();
        Assert.assertEquals(projectChanged.getStatus(), COMPLETED);
    }

    @Test
    public void listProject() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken);
        request.setSort(Sort.BY_NAME);
        @Nullable final List<ProjectDTO> projectList = projectEndpoint.listProject(request).getProjects();
        Assert.assertTrue(projectList.size() >= 1);
    }

    @Test
    public void removeByIdProject() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken);
        request.setProjectId(testProject.getId());
        projectEndpoint.removeByIdProject(request);
        Assert.assertNull(projectFindById(testProject.getId()));
    }

    @Test
    public void removeByIndexProject() {
        @NotNull final ProjectDTO projectFound = projectFindByIndex(1);
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(userToken);
        request.setIndex(1);
        projectEndpoint.removeByIndexProject(request);
        Assert.assertNull(projectFindById(projectFound.getId()));
    }

    @Test
    public void projectStartById() {
        @NotNull final ProjectChangeStatusByIdRequest requestChange = new ProjectChangeStatusByIdRequest(userToken);
        requestChange.setProjectId(testProject.getId());
        requestChange.setStatus(NOT_STARTED);
        projectEndpoint.changeStatusByIdProject(requestChange);
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(userToken);
        request.setProjectId(testProject.getId());
        projectEndpoint.startByIdProject(request);
        @Nullable final ProjectDTO projectFounded = projectFindById(testProject.getId());
        Assert.assertEquals(projectFounded.getStatus(), IN_PROGRESS);
    }

    @Test
    public void projectStartByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest requestChange = new ProjectChangeStatusByIndexRequest(userToken);
        requestChange.setIndex(1);
        requestChange.setStatus(NOT_STARTED);
        projectEndpoint.changeStatusByIndexProject(requestChange);
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(userToken);
        request.setIndex(1);
        final ProjectDTO projectChanged = projectEndpoint.startByIndexProject(request).getProject();
        Assert.assertEquals(projectChanged.getStatus(), IN_PROGRESS);
    }

    @Test
    public void projectUpdateById() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken);
        request.setProjectId(testProject.getId());
        request.setName(testProject.getName() + "1");
        request.setDescription(testProject.getDescription() + "1");
        projectEndpoint.updateByIdProject(request);
        @Nullable final ProjectDTO projectFounded = projectFindById(testProject.getId());
        Assert.assertEquals(projectFounded.getName(), testProject.getName() + "1");
        Assert.assertEquals(projectFounded.getDescription(), testProject.getDescription() + "1");
    }

    @Test
    public void projectUpdateByIndex() {
        @Nullable final ProjectDTO projectToChange = projectFindByIndex(1);
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(userToken);
        request.setIndex(1);
        request.setName(projectToChange.getName() + "1");
        request.setDescription(projectToChange.getDescription() + "1");
        final ProjectDTO projectChanged = projectEndpoint.updateByIndexProject(request).getProject();
        Assert.assertEquals(projectChanged.getName(), projectToChange.getName() + "1");
        Assert.assertEquals(projectChanged.getDescription(), projectToChange.getDescription() + "1");
    }

}
