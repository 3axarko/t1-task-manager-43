package ru.t1.zkovalenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

}
