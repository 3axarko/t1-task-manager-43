package ru.t1.zkovalenko.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.component.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
