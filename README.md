## TASK-MANAGER

## DEVELOPER

**NAME**: Kovalenko Zakhar

**E-MAIL**: 3axarko@gmail.com

**FAX**: None

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: WIN 10

## HARDWARE

**CPU**: i5
**RAM**: 16Gb

## APPLICATION BUILD

```bash
mvn clean install
```

## APPLICATION RUN

```bash
java -jar ./taksmanager.jar
```