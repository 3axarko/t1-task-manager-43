package ru.t1.zkovalenko.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.repository.model.ITaskRepository;
import ru.t1.zkovalenko.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return this.getClazz().getSimpleName();
    }

    @NotNull
    @Override
    protected Class<Task> getClazz() {
        return Task.class;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = String.format("FROM %s where project.id = :projectId", getEntityName());
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId) {
        @NotNull final String jpql = String.format("DELETE FROM %s where project.id = :projectId", getEntityName());
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

}