package ru.t1.zkovalenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.dto.model.UserDTO;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.util.HashUtil;

import javax.persistence.EntityManager;

public final class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return this.getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<UserDTO> getClazz() {
        return UserDTO.class;
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password
    ) {
        return create(propertyService, login, password, null, null);
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email
    ) {
        return create(propertyService, login, password, email, null);
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        return create(propertyService, login, password, null, role);
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @Nullable final String saltPass = HashUtil.salt(propertyService, password);
        user.setPasswordHash(saltPass == null ? "" : saltPass);
        if (role != null) user.setRole(role);
        if (email != null) user.setEmail(email);
        return add(user);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String jpql = String.format("FROM %s where login = :login", getEntityName());
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) {
        @NotNull final String jpql = String.format("FROM %s where email = :email", getEntityName());
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
