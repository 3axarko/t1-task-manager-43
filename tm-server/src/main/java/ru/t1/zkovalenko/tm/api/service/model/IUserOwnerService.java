package ru.t1.zkovalenko.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.enumerated.Sort;
import ru.t1.zkovalenko.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerService<M extends AbstractUserOwnerModel> extends IService<M> {

    void clear(@NotNull String userId);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Comparator comparator);

    @NotNull List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeByIndex(@NotNull String userId, @NotNull Integer index);

}
