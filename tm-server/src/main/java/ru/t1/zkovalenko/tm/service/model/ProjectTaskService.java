package ru.t1.zkovalenko.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.model.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.model.ITaskRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.model.IProjectTaskService;
import ru.t1.zkovalenko.tm.exception.entity.ProjectNotFoundException;
import ru.t1.zkovalenko.tm.exception.entity.TaskNotFoundException;
import ru.t1.zkovalenko.tm.exception.field.IndexIncorrectException;
import ru.t1.zkovalenko.tm.exception.field.ProjectEmptyException;
import ru.t1.zkovalenko.tm.exception.field.TaskEmptyException;
import ru.t1.zkovalenko.tm.exception.field.UserIdEmptyException;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.repository.model.ProjectRepository;
import ru.t1.zkovalenko.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public ITaskRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @NotNull
    @Override
    public IProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskEmptyException();
        @NotNull EntityManager entityManager = getEntityManager();
        try {
            IProjectRepository projectRepository = getProjectRepository(entityManager);
            if (projectRepository.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
            ITaskRepository taskRepository = getTaskRepository(entityManager);
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            @Nullable final Project project = projectRepository.findOneById(projectId);
            task.setProject(project);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectEmptyException();
        @NotNull EntityManager entityManager = getEntityManager();
        try {
            IProjectRepository projectRepository = getProjectRepository(entityManager);
            @Nullable final Project project = projectRepository.findOneById(projectId);
            if (project == null) throw new ProjectNotFoundException();
            ITaskRepository taskRepository = getTaskRepository(entityManager);
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, project.getId());
            entityManager.getTransaction().begin();
            tasks.forEach(taskRepository::remove);
            projectRepository.remove(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            @Nullable Project project = projectRepository.findOneByIndex(index);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(entityManager);
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, project.getId());
            entityManager.getTransaction().begin();
            tasks.forEach(taskRepository::remove);
            projectRepository.remove(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskEmptyException();
        @NotNull EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            if (projectRepository.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(entityManager);
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProject(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
