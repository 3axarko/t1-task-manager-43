package ru.t1.zkovalenko.tm.constant.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.model.Project;

public final class ProjectTestData {

    @NotNull
    public final static String PROJECT_NAME = "PROJECT_NAME";

    @NotNull
    public final static String PROJECT_DESCRIPTION = "PROJECT_DESCRIPTION";

    @NotNull
    public final static Project PROJECT1 = new Project();

    @NotNull
    public final static Project PROJECT2 = new Project();

    static {
        PROJECT1.setName(PROJECT_NAME);
        PROJECT1.setDescription(PROJECT_DESCRIPTION);
    }

}
