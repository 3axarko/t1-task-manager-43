package ru.t1.zkovalenko.tm.dtoService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.dto.IProjectDtoService;
import ru.t1.zkovalenko.tm.dto.model.ProjectDTO;
import ru.t1.zkovalenko.tm.marker.UnitServiceCategory;
import ru.t1.zkovalenko.tm.service.ConnectionService;
import ru.t1.zkovalenko.tm.service.PropertyService;
import ru.t1.zkovalenko.tm.service.dto.ProjectDtoService;

import static ru.t1.zkovalenko.tm.constant.model.ProjectTestData.*;
import static ru.t1.zkovalenko.tm.constant.model.UserTestData.USER1;
import static ru.t1.zkovalenko.tm.enumerated.Status.COMPLETED;
import static ru.t1.zkovalenko.tm.enumerated.Status.NOT_STARTED;

@Category(UnitServiceCategory.class)
public final class ProjectDtoServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectDtoService projectService = new ProjectDtoService(connectionService);

    @Nullable
    private ProjectDTO projectCreated;

    @After
    public void after() {
        if (projectCreated != null) projectService.remove(projectCreated);
        projectCreated = null;
    }

    @Test
    public void changeProjectStatusById() {
        projectCreated = projectService.create(USER1.getId(), PROJECT1.getId());
        Assert.assertEquals(projectCreated.getStatus(), NOT_STARTED);
        @NotNull final ProjectDTO projectChanged = projectService
                .changeProjectStatusById(USER1.getId(), projectCreated.getId(), COMPLETED);
        Assert.assertEquals(projectChanged.getStatus(), COMPLETED);
    }

    @Test
    public void changeProjectStatusByIndex() {
        projectCreated = projectService.create(USER1.getId(), PROJECT1.getId());
        Assert.assertEquals(projectCreated.getStatus(), NOT_STARTED);
        @NotNull final ProjectDTO projectChanged = projectService
                .changeProjectStatusByIndex(USER1.getId(), 1, COMPLETED);
        Assert.assertEquals(projectChanged.getStatus(), COMPLETED);
    }

    @Test
    public void createUserIdName() {
        projectCreated = projectService.create(USER1.getId(), PROJECT_NAME);
        @NotNull final ProjectDTO projectFounded = projectService.findOneById(projectCreated.getId());
        Assert.assertEquals(PROJECT_NAME, projectFounded.getName());
    }

    @Test
    public void createUserIdNameDescription() {
        projectCreated = projectService.create(USER1.getId(), PROJECT_NAME, PROJECT_DESCRIPTION);
        @NotNull final ProjectDTO projectFounded = projectService.findOneById(projectCreated.getId());
        Assert.assertEquals(PROJECT_NAME, projectFounded.getName());
        Assert.assertEquals(PROJECT_DESCRIPTION, projectFounded.getDescription());
    }

    @Test
    public void updateById() {
        projectCreated = projectService.create(USER1.getId(), PROJECT_NAME, PROJECT_DESCRIPTION);
        projectService.updateById(USER1.getId(),
                projectCreated.getId(),
                PROJECT_NAME + "1",
                PROJECT_DESCRIPTION + "1");
        @NotNull final ProjectDTO projectFounded = projectService.findOneById(projectCreated.getId());
        Assert.assertEquals(projectFounded.getName(), PROJECT_NAME + "1");
        Assert.assertEquals(projectFounded.getDescription(), PROJECT_DESCRIPTION + "1");
    }

    @Test
    public void updateByIndex() {
        projectCreated = projectService.create(USER1.getId(), PROJECT_NAME, PROJECT_DESCRIPTION);
        @NotNull final ProjectDTO projectFounded = projectService.findOneByIndex(1);
        @NotNull final ProjectDTO projectChanged = projectService.updateByIndex(projectFounded.getUserId(),
                1,
                PROJECT_NAME + "1",
                PROJECT_DESCRIPTION + "1");
        Assert.assertEquals(projectChanged.getName(), PROJECT_NAME + "1");
        Assert.assertEquals(projectChanged.getDescription(), PROJECT_DESCRIPTION + "1");
    }

}
