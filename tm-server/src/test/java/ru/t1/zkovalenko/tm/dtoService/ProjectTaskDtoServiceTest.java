package ru.t1.zkovalenko.tm.dtoService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.dto.IProjectDtoService;
import ru.t1.zkovalenko.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.zkovalenko.tm.api.service.dto.ITaskDtoService;
import ru.t1.zkovalenko.tm.dto.model.ProjectDTO;
import ru.t1.zkovalenko.tm.dto.model.TaskDTO;
import ru.t1.zkovalenko.tm.marker.UnitServiceCategory;
import ru.t1.zkovalenko.tm.service.ConnectionService;
import ru.t1.zkovalenko.tm.service.PropertyService;
import ru.t1.zkovalenko.tm.service.dto.ProjectDtoService;
import ru.t1.zkovalenko.tm.service.dto.ProjectDtoTaskService;
import ru.t1.zkovalenko.tm.service.dto.TaskDtoService;

import java.util.List;

import static ru.t1.zkovalenko.tm.constant.model.ProjectTestData.PROJECT_NAME;
import static ru.t1.zkovalenko.tm.constant.model.TaskTestData.TASK_NAME;
import static ru.t1.zkovalenko.tm.constant.model.UserTestData.USER1;

@Category(UnitServiceCategory.class)
public class ProjectTaskDtoServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskDtoService taskService = new TaskDtoService(connectionService);

    @NotNull
    private final IProjectDtoService projectService = new ProjectDtoService(connectionService);

    @NotNull
    private final IProjectTaskDtoService projectTaskService = new ProjectDtoTaskService(connectionService);

    @Nullable
    private ProjectDTO projectCreated;

    @Nullable
    private TaskDTO taskCreated;

    @Nullable
    private TaskDTO taskFounded;

    @Before
    public void addProjectTask() {
        projectCreated = projectService.create(USER1.getId(), PROJECT_NAME);
        taskCreated = taskService.create(USER1.getId(), TASK_NAME);
        projectTaskService.bindTaskToProject(USER1.getId(), projectCreated.getId(), taskCreated.getId());
        taskFounded = taskService.findAllByProjectId(USER1.getId(), projectCreated.getId()).get(0);
    }

    @After
    public void after() {
        if (taskCreated != null) taskService.remove(taskCreated);
        taskCreated = null;
        if (projectCreated != null) projectService.remove(projectCreated);
        projectCreated = null;
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertEquals(taskCreated.getId(), taskFounded.getId());
    }

    @Test
    public void removeProjectById() {
        Assert.assertNotNull(taskFounded);
        projectTaskService.removeProjectById(USER1.getId(), projectCreated.getId());
        Assert.assertNull(taskService.findOneById(taskFounded.getId()));
        projectCreated = null;
    }

    @Test
    public void removeProjectByIndex() {
        @NotNull final List<ProjectDTO> projects = projectService.findAll();
        @NotNull Integer index = 1;
        for (ProjectDTO project : projects) {
            if (project.getId().equals(projectCreated.getId())) break;
            index++;
        }
        projectTaskService.removeProjectByIndex(USER1.getId(), index);
        Assert.assertNull(taskService.findOneById(taskFounded.getId()));
        taskCreated = null;
        projectCreated = null;
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertNotNull(taskFounded);
        projectTaskService.unbindTaskFromProject(USER1.getId(), projectCreated.getId(), taskCreated.getId());
        final int taskSize = taskService.findAllByProjectId(USER1.getId(), projectCreated.getId()).size();
        Assert.assertEquals(0, taskSize);
    }

}
