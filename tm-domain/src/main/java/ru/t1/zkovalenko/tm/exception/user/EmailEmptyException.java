package ru.t1.zkovalenko.tm.exception.user;

public final class EmailEmptyException extends AbstractUserException {

    public EmailEmptyException() {
        super("Email is empty");
    }

}
