package ru.t1.zkovalenko.tm.exception.entity;

import lombok.NoArgsConstructor;
import ru.t1.zkovalenko.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractEntityException extends AbstractException {

    public AbstractEntityException(String message) {
        super(message);
    }

    public AbstractEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(Throwable cause) {
        super(cause);
    }

    public AbstractEntityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
