package ru.t1.zkovalenko.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUserOwnerModel extends AbstractModel {

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;

}
