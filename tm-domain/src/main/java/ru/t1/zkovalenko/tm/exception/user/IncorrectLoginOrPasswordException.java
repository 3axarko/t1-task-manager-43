package ru.t1.zkovalenko.tm.exception.user;

public final class IncorrectLoginOrPasswordException extends AbstractUserException {

    public IncorrectLoginOrPasswordException() {
        super("Login or Password is incorrect");
    }

}
